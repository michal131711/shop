<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Table;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 * @Table(name="attachment")
 */
class Attachment
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank(message="Plik musi mieć JPG/PNG format.")
     * @Assert\File(mimeTypes={"image/png", "image/jpeg"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $extension;

    public static function register(string $name, string $extension)
    {
        $attachment = new self();
        $attachment->name = $name;
        $attachment->extension = $extension;

        return $attachment;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getExtension(): ?string
    {
        return $this->extension;
    }

    /**
     * @param string $extension
     * @return Attachment
     */
    public function setExtension(string $extension): self
    {
        $this->extension = $extension;

        return $this;
    }
}
