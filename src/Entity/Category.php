<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Table;

/**
 * @ORM\Entity()
 * @Table(name="category")
 */
class Category
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=210)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Products", mappedBy="categories")
     */
    private $products;

    public static function register(string $name)
    {
        $category = new self();
        $category->name = $name;

        return $category;
    }

    /**
     * Category constructor.
     */
    public function __construct()
    {
        $this->products = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return string|null
     */
    public function setName(string $name): ?string
    {
        return $this->name = $name;
    }

    /**
     * @return array
     */
    public function getProducts(): array
    {
        return $this->products->map(function ($products) {
            return $products;
        })->toArray();
    }

    /**
     * @param Products $product
     * @return Category
     */
    public function addProduct(Products $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
            $product->addCategory($this);
        }

        return $this;
    }

    /**
     * @param Products $product
     * @return Category
     */
    public function removeProduct(Products $product): self
    {
        if ($this->products->contains($product)) {
            $this->products->removeElement($product);
            $product->removeCategory($this);
        }

        return $this;
    }
}
