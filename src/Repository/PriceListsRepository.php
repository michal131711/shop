<?php

namespace App\Repository;

use App\Entity\PriceLists;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PriceLists|null find($id, $lockMode = null, $lockVersion = null)
 * @method PriceLists|null findOneBy(array $criteria, array $orderBy = null)
 * @method PriceLists[]    findAll()
 * @method PriceLists[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PriceListsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PriceLists::class);
    }

    // /**
    //  * @return PriceLists[] Returns an array of PriceLists objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PriceLists
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
