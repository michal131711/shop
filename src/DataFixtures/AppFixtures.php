<?php

namespace App\DataFixtures;

use App\Dtos\UserDto;
use App\Entity\User;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
         $product = new User();
         $product->setName('sss');
         $product->setSurname('sss');
         $product->setEmail('ss@s.pl');
         $product->setPassword('sss');
         $product->setCreatedAt(new \DateTime());
         $product->setUpdatedAt(new DateTime());
        $userDto = new UserDto($product);
         $manager->persist($userDto);
        $manager->flush();

    }
}
