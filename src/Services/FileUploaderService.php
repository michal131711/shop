<?php
/**
 * Created by PhpStorm.
 * User: micha
 * Date: 18.04.19
 * Time: 09:33
 */

namespace App\Services;

use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploaderService
{
    private $targetDirectory;

    /**
     * FileUploaderService constructor.
     * @param $targetDirectory
     */
    public function __construct($targetDirectory)
    {
        $this->targetDirectory = $targetDirectory;
    }

    /**
     * @var UploadedFile $file
     * @return string|null
     */
    public function upload(UploadedFile $file): ?string
    {
        $fileName = $this->generateUniqueFileName() . '_' . $file->getClientOriginalName();
        try {
            $file->move($this->getTargetDirectory(), $fileName);
        } catch (FileException $e) {
            $e->getMessage();
        }
        return $fileName;
    }

    /**
     * @return mixed
     */
    public function getTargetDirectory()
    {
        return $this->targetDirectory;
    }

    private function generateUniqueFileName()
    {
        return md5(uniqid('', true));
    }
}
