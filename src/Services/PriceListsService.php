<?php
/**
 * Created by PhpStorm.
 * User: micha
 * Date: 16.04.19
 * Time: 09:56
 */

namespace App\Services;


use App\Entity\PriceLists;
use Doctrine\ORM\EntityManagerInterface;

class PriceListsService
{
    private $managerRegistry;

    /**
     * PriceListsService constructor.
     * @param $managerRegistry
     */
    public function __construct(EntityManagerInterface $managerRegistry)
    {
        $this->managerRegistry = $managerRegistry;
    }

    /**
     * @return PriceLists|PriceLists[]|object[]
     */
    public function getAllPriceLists()
    {
        return $this->managerRegistry->getRepository(PriceLists::class)->findAll();
    }

    /**
     * @param int $id
     * @return PriceLists|object|null
     */
    public function getPriceLists(int $id)
    {
        return $this->managerRegistry->getRepository(PriceLists::class)->find($id);
    }

    /**
     * @param PriceLists $user
     * @return EntityManagerInterface
     */
    public function removePriceLists(PriceLists $user): EntityManagerInterface
    {
        $this->managerRegistry->remove($user);

        return $this->managerRegistry;
    }
}
