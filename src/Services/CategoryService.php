<?php
/**
 * Created by PhpStorm.
 * User: micha
 * Date: 19.04.19
 * Time: 19:21
 */

namespace App\Services;


use App\Entity\Category;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;

class CategoryService
{
    private $categoryRepository;
    private $managerRegistry;

    /**
     * CategoryService constructor.
     * @param CategoryRepository $categoryRepository
     * @param EntityManagerInterface $managerRegistry
     */
    public function __construct(CategoryRepository $categoryRepository, EntityManagerInterface $managerRegistry)
    {
        $this->categoryRepository = $categoryRepository;
        $this->managerRegistry = $managerRegistry;
    }

    /**
     * @return Category|Category[]|object[]
     */
    public function getAllCategories()
    {
        return $this->categoryRepository->findAll();
    }

    /**
     * @param int $id
     * @return Category|object|null
     */
    public function getCategory(int $id)
    {
        return $this->categoryRepository->find($id);
    }

    /**
     * @param Category $category
     * @return EntityManagerInterface
     */
    public function removeCategory(Category $category): EntityManagerInterface
    {
        $this->managerRegistry->remove($category);

        return $this->managerRegistry;
    }
}
