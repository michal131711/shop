<?php
/**
 * Created by PhpStorm.
 * User: micha
 * Date: 19.04.19
 * Time: 19:21
 */

namespace App\Services;


use App\Entity\Products;
use App\Repository\ProductsRepository;
use Doctrine\ORM\EntityManagerInterface;

class ProductsService
{
    private $productRepository;
    private $managerRegistry;

    /**
     * ProductsService constructor.
     * @param ProductsRepository $productRepository
     * @param EntityManagerInterface $managerRegistry
     */
    public function __construct(ProductsRepository $productRepository, EntityManagerInterface $managerRegistry)
    {
        $this->productRepository = $productRepository;
        $this->managerRegistry = $managerRegistry;
    }

    public function getAllProducts()
    {
        return $this->productRepository->findAll();
    }

    /**
     * @param int $id
     * @return Products|object|null
     */
    public function getProduct(int $id)
    {
        return $this->productRepository->find($id);
    }

    /**
     * @param Products $product
     * @return EntityManagerInterface
     */
    public function removeProduct(Products $product): EntityManagerInterface
    {
        $this->managerRegistry->remove($product);

        return $this->managerRegistry;
    }
}
