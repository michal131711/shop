<?php
/**
 * Created by PhpStorm.
 * User: micha
 * Date: 18.04.19
 * Time: 07:56
 */

namespace App\Services;


use App\Entity\Attachment;
use App\Repository\AttachmentRepository;
use Doctrine\ORM\EntityManagerInterface;

class AttachmantService
{
    private $attachmantRepository;
    private $managerRegistry;

    /**
     * AttachmantService constructor.
     * @param AttachmentRepository $attachmantRepository
     * @param EntityManagerInterface $managerRegistry
     */
    public function __construct(AttachmentRepository $attachmantRepository,EntityManagerInterface $managerRegistry)
    {
        $this->attachmantRepository = $attachmantRepository;
        $this->managerRegistry = $managerRegistry;
    }

    /**
     * @return Attachment|Attachment[]|object[]
     */
    public function getAllAttachments()
    {
        return $this->attachmantRepository->findAll();
    }

    /**
     * @param int $id
     * @return Attachment|object|null
     */
    public function getAttachment(Attachment $id)
    {
        return $this->attachmantRepository->find($id);
    }

    /**
     * @param Attachment $attachment
     * @return EntityManagerInterface
     */
    public function removeAttachment(Attachment $attachment): EntityManagerInterface
    {
        $this->managerRegistry->remove($attachment);

        return $this->managerRegistry;
    }
}
