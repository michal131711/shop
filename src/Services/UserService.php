<?php
/**
 * Created by PhpStorm.
 * User: micha
 * Date: 16.04.19
 * Time: 09:56
 */

namespace App\Services;


use App\Dtos\UserDto;
use App\Entity\User;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;

class UserService
{
    private $managerRegistry;

    /**
     * UserService constructor.
     * @param $managerRegistry
     */
    public function __construct(EntityManagerInterface $managerRegistry)
    {
        $this->managerRegistry = $managerRegistry;
    }

    /**
     * @return User|User[]|object[]
     */
    public function getAllUsers()
    {
        return $this->managerRegistry->getRepository(User::class)->findAll();
    }

    /**
     * @param int $id
     * @return User|object|null
     */
    public function getUser(int $id)
    {
        return $this->managerRegistry->getRepository(User::class)->find($id);
    }

    /**
     * @param User $user
     * @return EntityManagerInterface
     */
    public function removeUser(User $user): EntityManagerInterface
    {
        $this->managerRegistry->remove($user);

        return $this->managerRegistry;
    }
}
