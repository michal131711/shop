<?php

namespace App\Dtos;

use App\Entity\Category;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class CategoryDto
{
    /**
     * @Serializer\Type(name="string")
     * @Assert\NotBlank(message="Tytuł nie może być pusty")
     */
    private $name;

    /**
     * CategoryDto constructor.
     * @param Category $category
     */
    public function __construct(Category $category)
    {
        $this->name = $category->getName();
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
