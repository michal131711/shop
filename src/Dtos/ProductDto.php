<?php


namespace App\Dtos;

use JMS\Serializer\Annotation as Serializer;
use App\Entity\Products;
use Symfony\Component\Validator\Constraints as Assert;

class ProductDto
{
    /**
     * @Serializer\Type(name="string")
     * @Assert\NotBlank(message="Tytuł nie może być pusty")
     */
    private $name;

    /**
     * @Serializer\Type(name="string")
     * @Assert\NotBlank(message="Opis nie może być pusty")
     */
    private $description;

    /**
     * @Serializer\Type(name="array")
     */
    private $categories;

    /**
     * @param Products $products
     */
    public function __construct(Products $products)
    {
        $this->name = $products->getName();
        $this->description = $products->getDescription();
        $this->categories = $products->getCategory();
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param mixed $categories
     */
    public function setCategories($categories): void
    {
        $this->categories = $categories;
    }

//    /**
//     * @return array
//     */
//    public function getCategory(): array
//    {
//        return $this->categories->map(function ($categories) {
//            return $categories;
//        })->toArray();
//    }

//    /**
//     * @param Category $category
//     * @return Products
//     */
//    public function addCategory(Category $category): self
//    {
//        if (!$this->categories->contains($category)) {
//            $this->categories[] = $category;
//        }
//
//        return $this;
//    }

//    /**
//     * @param Category $category
//     * @return Products
//     */
//    public function removeCategory(Category $category): self
//    {
//        if ($this->categories->contains($category)) {
//            $this->categories->removeElement($category);
//        }
//
//        return $this;
//    }
}
