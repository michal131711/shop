<?php

namespace App\Dtos;

use App\Entity\User;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class UserDto
{
    use TimestampableEntity;

    /**
     * @Serializer\Type(name="string")
     * @Assert\NotBlank(message="Imię nie może  być puste")
     */
    private $name;

    /**
     * @Serializer\Type(name="string")
     * @Assert\NotBlank(message="Nazwisko nie może być puste")
     */
    private $surname;

    /**
     * @Serializer\Type(name="string")
     * @Assert\NotBlank(message="Pole email może być puste")
     * @Assert\Email(message="To nie jest adres e-mail {{ value }}.")
     */
    private $email;

    /**
     * @Serializer\Type(name="string")
     * @Assert\NotBlank(message="Hasło nie może być puste")
     */
    private $password;

    /**
     * @Serializer\Type(name="string")
     */
    private $roles;

    /**
     * UserDto constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->name = $user->getName();
        $this->surname = $user->getSurname();
        $this->email = $user->getEmail();
        $this->password = $user->getPassword();
        $this->roles = $user->getRoles();
        $this->createdAt = $user->getCreatedAt();
        $this->updatedAt = $user->getUpdatedAt();
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return string|null
     */
    public function getSurname(): ?string
    {
        return $this->surname;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @return mixed
     */
    public function getRoles()
    {
        return $this->roles;
    }
}
