<?php

namespace App\handlers;

use App\Dtos\ProductDto;
use App\Entity\Products;

class ProductRegisterHandler
{
    public function handle(ProductDto $productDto)
    {
        return Products::register(
            $productDto->getName(),
            $productDto->getDescription(),
            $productDto->getCategories()
        );
    }
}
