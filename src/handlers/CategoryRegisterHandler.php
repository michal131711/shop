<?php


namespace App\handlers;


use App\Dtos\CategoryDto;
use App\Entity\Category;

class CategoryRegisterHandler
{
    public function handle(CategoryDto $categoryDto)
    {
        return Category::register(
            $categoryDto->getName()
        );
    }
}
