<?php

namespace App\handlers;

use App\Dtos\UserDto;
use App\Entity\User;

class UserRegisterHandler
{
    public function handle(UserDto $userDto)
    {
        return User::register(
            $userDto->getName(),
            $userDto->getSurname(),
            $userDto->getEmail(),
            $userDto->getPassword(),
            $userDto->getRoles()
        );
    }
}
