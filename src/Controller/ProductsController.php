<?php

namespace App\Controller;

use App\Dtos\ProductDto;
use App\Form\ProductsType;
use App\Form\ProductUpdateType;
use App\handlers\ProductRegisterHandler;
use App\Services\ProductsService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use http\Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/api/products")
 */
class ProductsController extends AbstractFOSRestController
{
    private $productService;

    /**
     * ProductsController constructor.
     * @param $productService
     */
    public function __construct(ProductsService $productService)
    {
        $this->productService = $productService;
    }

    /**
     * @Rest\Get("")
     */
    public function index(): JsonResponse
    {
        $getAllProducts = $this->productService->getAllProducts();

        return new JsonResponse($getAllProducts, Response::HTTP_OK);
    }

    /**
     * @Rest\Post("/new")
     * @param Request $request
     * @param ValidatorInterface $validator
     * @param ProductDto $productDto
     * @return JsonResponse
     * @ParamConverter("productDto", converter="fos_rest.request_body")
     */
    public function new(Request $request, ValidatorInterface $validator, ProductDto $productDto): JsonResponse
    {
        try {
            $handleProduct = new ProductRegisterHandler();
            $form = $this->createForm(ProductsType::class, $productDto);
            $form->handleRequest($request);
            $form->submit($request->request->all());
            $errors = $validator->validate($productDto);

            if (count($errors) > 0) {
                return new JsonResponse($errors, Response::HTTP_BAD_REQUEST);
            }

            $saveProduct = $handleProduct->handle($productDto);
            $em = $this->getDoctrine()->getManager();
            $em->persist($saveProduct);
            $em->flush();

            return new JsonResponse([
                'success' => 'Stworzono rekord'
            ], 201, [
                'Content-Type' => 'application/json'
            ]);

        } catch (Exception $exception) {
            dd($exception->getMessage());
        }
    }

    /**
     * @Rest\Get("/{id}", name="products_show", options={"expose"=true})
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        $getProduct = $this->productService->getProduct($id);

        return new JsonResponse($getProduct);
    }

    /**
     * @Rest\Put("/{id}/edit")
     * @param Request $request
     * @param int $id
     * @param ValidatorInterface $validator
     * @return Response
     */
    public function edit(Request $request, int $id, ValidatorInterface $validator): Response
    {
        $product = $this->productService->getProduct($id);
        $form = $this->createForm(ProductUpdateType::class, $product);
        $form->submit($request->request->all());
        $errors = $validator->validate($product);

        if (count($errors) > 0) {
            return new JsonResponse($errors, Response::HTTP_BAD_REQUEST);
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($product);
        $em->flush();

        return new JsonResponse(['updated' => 'Zmieniono rekord'], 201, ['Content-Type' => 'application/json']);
    }

    /**
     * @Rest\Delete("/{id}")
     * @param int $id
     * @return Response
     */
    public function delete(int $id): Response
    {
        $getProduct = $this->productService->getProduct($id);
        $this->productService->removeProduct($getProduct)->flush();

        return new JsonResponse(['success'], Response::HTTP_NO_CONTENT, ['Content-Type' => 'application/json']);
    }
}
