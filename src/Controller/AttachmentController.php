<?php

namespace App\Controller;

use App\Dtos\AttachmantDto;
use App\Entity\Attachment;
use App\Form\AttachmentType;
use App\Services\AttachmantService;
use App\Services\FileUploaderService;
use DateTime;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/api/attachment")
 */
class AttachmentController extends AbstractFOSRestController
{
    private $attachmantService;

    /**
     * AttachmentController constructor.
     * @param $attachmantService
     */
    public function __construct(AttachmantService $attachmantService)
    {
        $this->attachmantService = $attachmantService;
    }

    /**
     * @Rest\Get("")
     */
    public function index(): JsonResponse
    {
        $getAllAttachmant = $this->attachmantService->getAllAttachments();

        return $this->json($this->view($getAllAttachmant));
    }

    /**
     * @Rest\Post("/new")
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function new(Request $request, ValidatorInterface $validator): Response
    {
        $attachmant = new Attachment();
        $form = $this->createForm(AttachmentType::class, $attachmant);
        $form->handleRequest($request);
        $form->submit($request->files->all());

        $errors = $validator->validate($attachmant);

        if (count($errors) > 0) {
            return $this->handleView($this->view($errors));
        }

        $fileUploader = new FileUploaderService($this->getParameter('files_directory'));

        $file = $attachmant->getName();
        $fileName = $fileUploader->upload($file);

        $attachmant->setName($fileName);
        $attachmant->setExtension($file->getClientOriginalExtension());
        $attachmant->setCreatedAt(new DateTime());
        $attachmant->setUpdatedAt(new DateTime());
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($attachmant);
        $entityManager->flush();

        return new JsonResponse('success', 201);
    }

    /**
     * @Rest\Get("/{id}")
     * @param Attachment $id
     * @return JsonResponse
     */
    public function show(Attachment $id): JsonResponse
    {
        $getAttachmant = $this->attachmantService->getAttachment($id);

        return $this->json($this->view($getAttachmant->getName()));
    }

    /**
     * @Rest\Delete("/{id}")
     * @param int $id
     * @return Response
     */
    public function delete(int $id): Response
    {
        $attachment = $this->attachmantService->getAttachment($id);
        $fileUploader = $this->getParameter('files_directory');
        $pathFile = $attachment->getName();
        unlink("$fileUploader$pathFile");

        $this->attachmantService->removeAttachment($attachment)->flush();

        return new Response('remove', Response::HTTP_NO_CONTENT, ['Content-Type' => 'application/json']);
    }
}
