<?php

namespace App\Controller;

use App\Dtos\UserDto;
use App\Entity\User;
use App\Form\UserType;
use App\Form\UserUpdateType;
use App\handlers\UserRegisterHandler;
use App\Services\UserService;
use DateTime;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Model;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Swagger\Annotations as SWG;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * user controller.
 * @Route("/api/user")
 * @SWG\Response(
 *     response=200,
 *     description="Uzytkownik",
 *     @Model(type=User::class)
 * )
 */
class UserController extends AbstractFOSRestController
{
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @Rest\Get()
     * @return JsonResponse|Response
     * @throws NotFoundHttpException when does not exist
     */
    public function index(): JsonResponse
    {
        $getUsers = $this->userService->getAllUsers();

        return $this->json($this->view($getUsers));
    }

    /**
     * @Rest\Get("{id}")
     * @param int $id
     * @return JsonResponse|Response
     */
    public function show(int $id): JsonResponse
    {
        $getUser = $this->userService->getUser($id);

        return $this->json($this->view($getUser));
    }

    /**
     * @Rest\Post("/create", name="user.create")
     * @param Request $request
     * @param UserDto $userDto
     * @param ValidatorInterface $validator
     * @return JsonResponse|Response
     * @throws \Exception
     * @ParamConverter("userDto", converter="fos_rest.request_body")
     */
    public function create(Request $request, UserDto $userDto, ValidatorInterface $validator)
    {
        try {
            $handleUser = new UserRegisterHandler();
            $form = $this->createForm(UserType::class, $userDto);
            $form->handleRequest($request);
            $errors = $validator->validate($userDto);

            if (count($errors) > 0) {
                return $this->handleView($this->view($errors));
            }

            $saveUser = $handleUser->handle($userDto);
            $em = $this->getDoctrine()->getManager();
            $saveUser->setRoles("ROLE_USER");
            $saveUser->setCreatedAt(new DateTime());
            $saveUser->setUpdatedAt(new DateTime());
            $em->persist($saveUser);
            $em->flush();

            return new JsonResponse([
                'success' => 'Stworzono rekord'
            ],201, [
                'Content-Type' => 'application/json'
            ]);

        } catch (Exception $exception) {
            dd($exception->getMessage());
        }
    }

    /**
     * @Rest\Put("/update/{id}")
     * @param Request $request
     * @param int $id
     * @param ValidatorInterface $validator
     * @return Response
     * @throws \Exception
     * @ParamConverter("userDto", converter="fos_rest.request_body")
     */
    public function update(Request $request, int $id, ValidatorInterface $validator): Response
    {
        $user = $this->userService->getUser($id);
        $form = $this->createForm(UserUpdateType::class, $user);
        $form->submit($request->request->all());
        $errors = $validator->validate($user);

        if (count($errors) > 0) {
            return $this->handleView($this->view($errors));
        }

        $em = $this->getDoctrine()->getManager();
        $user->setRoles(
            $request->request->get('roles')
                ? $request->request->get('roles') : "ROLE_USER"
        );
        $user->setCreatedAt(new DateTime());
        $user->setUpdatedAt(new DateTime());

        $em->persist($user);
        $em->flush();

        return new JsonResponse(['updated' => 'Zmieniono rekord'], 201, ['Content-Type' => 'application/json']);
    }

    /**
     * @Route("/{id}", name="user.remove", methods={"DELETE"})
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        $user = $this->userService->getUser($id);
        $this->userService->removeUser($user)->flush();

        return $this->json(['success'], Response::HTTP_NO_CONTENT, ['Content-Type' => 'application/json']);
    }
}
