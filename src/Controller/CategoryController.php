<?php

namespace App\Controller;

use App\Dtos\CategoryDto;
use App\Entity\Category;
use App\Form\CategoryType;
use App\Form\CategoryUpdateType;
use App\handlers\CategoryRegisterHandler;
use App\Services\CategoryService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use http\Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/api/category")
 */
class CategoryController extends AbstractFOSRestController
{
    private $categoryService;

    /**
     * CategoryController constructor.
     * @param $categoryService
     */
    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    /**
     * @Rest\Get("")
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $getAllCategories = $this->categoryService->getAllCategories();

        return $this->json($this->view($getAllCategories));
    }

    /**
     * @Rest\Post("/new")
     * @param Request $request
     * @param CategoryDto $categoryDto
     * @param ValidatorInterface $validator
     * @return Response
     * @ParamConverter("categoryDto", converter="fos_rest.request_body")
     */
    public function new(Request $request, CategoryDto $categoryDto, ValidatorInterface $validator): Response
    {
        try {
            $handleCategory = new CategoryRegisterHandler();
            $form = $this->createForm(CategoryType::class, $categoryDto);
            $form->handleRequest($request);
            $errors = $validator->validate($categoryDto);

            if (count($errors) > 0) {
                return $this->handleView($this->view($errors));
            }

            $saveCategory = $handleCategory->handle($categoryDto);
            $em = $this->getDoctrine()->getManager();
            $em->persist($saveCategory);
            $em->flush();

            return new JsonResponse([
                'success' => 'Stworzono rekord'
            ], 201, [
                'Content-Type' => 'application/json'
            ]);

        } catch (Exception $exception) {
            dd($exception->getMessage());
        }
    }

    /**
     * @Rest\Put("/{id}/edit")
     * @param Request $request
     * @param int $id
     * @param ValidatorInterface $validator
     * @return Response
     */
    public function edit(Request $request, int $id, ValidatorInterface $validator): Response
    {
        $category = $this->categoryService->getCategory($id);
        $form = $this->createForm(CategoryUpdateType::class, $category);
        $form->submit($request->request->all());
        $errors = $validator->validate($category);

        if (count($errors) > 0) {
            return $this->handleView($this->view($errors));
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($category);
        $em->flush();

        return new JsonResponse(['updated' => 'Zmieniono rekord'], 201, ['Content-Type' => 'application/json']);
    }

    /**
     * @Rest\Delete("/{id}")
     * @param int $id
     * @return Response
     */
    public function delete(int $id): Response
    {
        $category = $this->categoryService->getCategory($id);
        $this->categoryService->removeCategory($category)->flush();

        return $this->json(['success'], Response::HTTP_NO_CONTENT, ['Content-Type' => 'application/json']);
    }
}
